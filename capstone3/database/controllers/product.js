const Product = require("../models/Product");

module.exports.addProduct = (data) => {
	if (data.isAdmin) {
		let newProduct = new Product({
			title : data.product.title,
			author : data.product.author,
			description : data.product.description,
			price : data.product.price,
			imgSource: data.product.imgSource
		});
		return newProduct.save().then((product, error) => {

			if (error) {
				return false;
			} else {
				return true;
			};
		});
	} else {
		return false;
	};
	

};

// Retrieve all products

module.exports.getAllProducts = (data) => {
	if(data.isAdmin){
		return Product.find({}).then(result => {
			return result;
		});
	}
	else {
		return "Only admins can do this";
	}
};

// Retrieve all ACTIVE products

module.exports.getAllActive = () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	});

};

// Retrieving a specific product

module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {
		return result;
	});

};

// Update a product

module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		title : reqBody.title,
		author : reqBody.author,
		description	: reqBody.description,
		price : reqBody.price,
		imgSource : reqBody.imgSource
	};

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
};

// Archive a product

module.exports.archiveProduct = (reqParams) => {

	let updateActiveField = {
		isActive : false
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

		if (error) {
			return false;
		} else {
			return true;
		}
	});
};

// Unarchive a product

module.exports.unarchiveProduct = (reqParams) => {

	let updateActiveField = {
		isActive : true
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

		if (error) {
			return false;
		} else {
			return true;
		}
	});
};