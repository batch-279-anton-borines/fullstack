// The "User" variable is defined using a capitalized letter to indicate that what we are using is the "User" model for code readability
const User = require("../models/User.js");
const Product = require("../models/Product.js");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.getAllUsers = ()=>{
	return User.find({}).then(result => {
		return result;
	})
}

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		if (result.length > 0) {
			return true;
		} else {
			return false;
		};
	});
};

// User registration

module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)
		// address : reqBody.address,
		// isFemale : reqBody.isFemale,
		// birthYear : reqBody.birthYear
	})

	return newUser.save().then((user, error) => {
		if (error) {
			return false;
		} 
		else {
			return true;
		};
	});
};

// User authentication

module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		if(result == null){
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if (isPasswordCorrect) {
				return { access : auth.createAccessToken(result) }
			} 
			else {
				return false;
			};
		};
	});
};

// Retrieve user details

module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	});
};

// Order an item

module.exports.order = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.orders.push({
			productId : data.productId,
			title : data.title,
			price : data.price
		});
		console.log(data.productId);
		// Saves the updated user information in the database
		return user.save().then((user, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});

	});
	console.log(user);
	let isProductUpdated = await Product.findById(data.productId).then(product => {

		product.buyers.push({userId : data.userId});
		return product.save().then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	});

	if(isUserUpdated && isProductUpdated){
		return true;
	} else {
		return false;
	};
};