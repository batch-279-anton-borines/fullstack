const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth");

// Route for creating a product
router.post("/", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.addProduct(data).then(resultFromController => res.send(resultFromController));

});

// Route for retrieving all the products
router.get("/all", (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.getAllProducts(data).then(resultFromController => res.send(resultFromController));

});

// Route for retrieving all the ACTIVE products

router.get("/", (req, res) => {

	productController.getAllActive().then(resultFromController => res.send(resultFromController));

});

// Route for retrieving a specific product

router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// Route for updating a product

router.put("/updateProduct/:productId", auth.verify, (req, res) => {

	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));

});

// Route to archiving a product

router.put("/:productId/archive", auth.verify, (req, res) => {

	productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
	
});

// Route to unarchiving a product

router.put("/:productId/unarchive", auth.verify, (req, res) => {

	productController.unarchiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
	
});

module.exports = router;