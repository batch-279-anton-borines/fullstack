const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const productController = require("../controllers/product");
const auth = require("../auth");


router.get("/all", (req, res)=>{
	taskController.getAllUsers().then(resultFromController => res.send(resultFromController));
})

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving user details

router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});

// Route for user to purchase an item
router.post("/order", auth.verify, (req, res) => {

	let data = {
		userId : auth.decode(req.headers.authorization).id,
		productId : req.body.productId,
		title : req.body.title,
		price : req.body.price
	}

	userController.order(data).then(resultFromController => res.send(resultFromController));

});

module.exports = router;