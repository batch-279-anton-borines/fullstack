const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	title : {
		type : String,
		required : [true, "Title is required"]
	},
	author : {
		type : String,
		required : [true, "Author is required"]
	},
	description : {
		type : String,
		required : [true, "Description is required"]
	},
	price : {
		type : Number,
		required : [true, "Price is required"]
	},
	imgSource : {
		type : String,
		required : [true, "Image is required"]
	},
	isActive : {
		type : Boolean,
		default : true
	},
	createdOn : {
		type : Date,
		default : new Date()
	},
	buyers : [
		{
			userId : {
				type : String,
				required: [true, "UserId is required"]
			},
			enrolledOn : {
				type : Date,
				default : new Date() 
			}
		}
	]
})

module.exports = mongoose.model("Product", productSchema);