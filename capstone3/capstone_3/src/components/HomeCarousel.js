import {Carousel, Image} from 'react-bootstrap';

export default function HomeCarousel() {
return (
    <div >
    <Carousel className='vh-75 w-100 d-inline-block mb-5 shadow' fade>
    <Carousel.Item>
        <Image
        className="d-block mx-auto img-fit  carousel-height carousel-caption-bg"
        src="https://images.unsplash.com/photo-1500204904030-0cac9aa912f2?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1470&q=80"
        alt="First slide"
        />
        <Carousel.Caption className='container d-flex flex-column align-items-center justify-content-center vh-25 '>
        <div  className='carousel-text'>
        <h1>Read on the go!</h1>
        <p>In the train, on the bus, in a car</p>
        </div>
        </Carousel.Caption>
    </Carousel.Item>
    <Carousel.Item>
        <Image
        className="d-block mx-auto img-fit carousel-height carousel-caption-bg"
        src="https://images.unsplash.com/photo-1472745433479-4556f22e32c2?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1469&q=80"
        alt="Second slide"
        />

        <Carousel.Caption className='container d-flex flex-column align-items-center justify-content-center vh-25'>
        <div  className='carousel-text'>
        <h1>Feed your mind.</h1>
        <p>Get your imaginative juices flowing by reading</p>
        </div>
        </Carousel.Caption>
    </Carousel.Item>
    <Carousel.Item>
        <Image
        className="d-block mx-auto img-fit carousel-height carousel-caption-bg"
        src="https://images.unsplash.com/photo-1456953180671-730de08edaa7?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1471&q=80"
        alt="Third slide"
        />

        <Carousel.Caption className='container d-flex flex-column align-items-center justify-content-center vh-25'>
        <div  className='carousel-text'>
        <h1>Kindle Ready!</h1>
        <p>
            eBook formats are kindle friendly for lightweight unlimited reading!
        </p>
        </div>
        </Carousel.Caption>
    </Carousel.Item>
    </Carousel>
    </div>
);
}
