import { useContext, useState, useEffect } from "react"
import UserContext from "../UserContext";
import {Navigate} from "react-router-dom";
import {Container, Col, Row, Button, Table, Modal, Form, Image} from "react-bootstrap"
import Swal from "sweetalert2";
import AppSideNav from "../components/AppSideNav";
import React from "react";
import logo from "../images/logo.png"
import HomeBanner from "../components/HomeBanner";

export default function AllProducts(){

     const data = {
		title: "VIEW ALL PRODUCTS",
		content: "Add, edit, or archive a product on this page.",
		destination: "/products/allProducts",
		label: "PRODUCTS",
		image: {logo}
	}

    const { user } = useContext(UserContext);

    const [allProducts, setAllProducts]= useState([]);

    const [productId, setProductId] = useState("");
    const [title, setTitle] = useState("");
    const [author, setAuthor] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    const [imgSource, setImgSource] = useState("");

    const [isActive, setIsActive] = useState(false);

    const [modalAdd, setModalAdd] = useState(false);
    const [modalEdit, setModalEdit] = useState(false);


    const addProductOpen = () => setModalAdd(true);
    const addProductClose = () => setModalAdd(false);

    const openEdit = (id) => {
        setProductId(id);

        fetch(`${process.env.REACT_APP_API_URL}/products/allProducts/updateProduct/${id}`)
            .then(res => res.json())
            .then(data => {

                console.log(data);

                setTitle(data.title);
                setAuthor(data.author);
                setDescription(data.description);
                setPrice(data.price);
                setImgSource(data.imgSource);
            });
     setModalEdit(true)
    };

    const closeEdit = () => {

        setTitle('');
        setAuthor('');
        setDescription('');
        setImgSource('');
        setPrice(0);

     setModalEdit(false);
    };

/* GET ALL PRODUCTS */
	const fetchData = () =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

            setAllProducts(data.map(products =>{
                console.log(products);
                return (
                    <>
                    
                    <tr key={products._id}>
                        <td>{products._id}</td>
                        <td>{products.title}</td>
                        <td>{products.author}</td>
                        <td>{products.description}</td>
                        <td>{products.price}</td>
                        <td>{products.isActive ? "Active" : "Inactive"}</td>
                        <td>
                            {(products.isActive)
                            ?
                            <Container fluid>
                            <Row>
                            <Button variant="danger" size="sm" onClick={() => archive(products._id, products.title)}>Archive</Button>
                            <Button variant="warning" className="mt-2"size="sm" onClick={() => openEdit(products._id)}>Edit</Button>
                            </Row>
                            </Container>
                            :
                            <>
                            <Container fluid>
                            <Row>
                            <Button variant="success" className="mx-1" size="sm" onClick={() => unarchive(products._id, products.title)}>Unarchive</Button>
                            <Button variant="secondary" className="mx-1" size="sm" onClick={() => openEdit(products._id)}>Edit</Button>
                            </Row>
                            </Container>

                            </>}
                        </td>
                    </tr>
                    </>
                )
            }));
		});
	}


    /* ARCHIVE PRODUCT */
    const archive = (id, title) =>{
        console.log(id);
        console.log(title);
        fetch(`${process.env.REACT_APP_API_URL}/products/allProducts/${id}/archive`,
        {
            method : "PUT",
            headers : {
                "Content-Type" : "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
        body: JSON.stringify({
            isActive: false
        })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data){
                Swal.fire({
                    title: "ARCHIVING SUCCESS!",
                    icon: "success",
                    text: `The product is now archived`
                });

                fetchData();
            }else{
                Swal.fire({
                    title: "Archive Unsuccessful",
                    icon: "error",
                    text: "Something went wrong. Please try again later"
                })
            }
        })
    }

    const unarchive = (id, name) =>{
        console.log(id);
        console.log(name);
        fetch(`${process.env.REACT_APP_API_URL}/products/allProducts/${id}/unarchive`,
        {
            method : "PUT",
            headers : {
                "Content-Type" : "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
        body: JSON.stringify({
            isActive: true
        })
    
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data){
                Swal.fire({
                    title: "Unarchive Successful",
                    icon: "success",
                    text: `The product is now Active`
                });

                fetchData();
            }else{
                Swal.fire({
                    title: "Unarchive Unsuccessful",
                    icon: "error",
                    text: "Something went wrong. Please try again later"
                })
            }
        })
    }

	useEffect(()=>{
		fetchData();


	}, [])

    /* ADD PRODUCT */
    const addProduct = (e) => {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/products/allProducts`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                title: title,
                author: author,
                description: description,
                price: price,
                imgSource: imgSource
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                if (data) {
                    Swal.fire({
                        title: "PRODUCT ADDED SUCCESSFULLY!",
                        icon: "success",
                        text: `"The new product was added to the product list.`
                    });
                    fetchData();
                    addProductClose();
                }
                else {
                    Swal.fire({
                        title: "ADD PRODUCT UNSSUCCESSFUL!",
                        icon: "error",
                        text: `The system is experiencing trouble at the moment. Please try again later.`
                    });
                    addProductClose();
                }
            })
        setTitle('');
        setAuthor('');
        setImgSource('');
        setDescription('');
        setPrice(0);
    }

    /* EDIT PRODUCT */
    const editProduct = (e) => {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/products/allProducts/updateProduct/${productId}`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                title: title,
                author: author,
                description: description,
                price: price,
                imgSource: imgSource
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                if (data) {
                    Swal.fire({
                        title: "PRODUCT EDIT SUCCESSFUL!",
                        icon: "success",
                        text: `Product was edited successfully.`
                    });
                    fetchData();
                    closeEdit();
                }
                else {
                    Swal.fire({
                        title: "PRODUCT EDIT UNSUCCESSFUL!",
                        icon: "error",
                        text: `The system is experiencing trouble at the moment. Please try again later.`
                    });
                    closeEdit();
                }
            })

        setTitle('');
        setAuthor('');
        setImgSource('');
        setDescription('');
        setPrice(0);
    }

    useEffect(() => {

        if (title != "" && description != "" && price > 0) {
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [title, description, price]);

	return(
        <>
         
        <Container fluid className="w-100 d-flex h-100 m-0 p-0">
            <Row className="w-100 m-0 p-0">
            <Col className="bg-white d-flex  pt-3 m-0 shadow" xs={12} md={2} lg={2}>
                <AppSideNav/>
            </Col>
       
            <Col className="colr-bg m-0 p-5 d-flex justify-content-center" xs={12} md={10} lg={10}>
                {
                    (user.isAdmin)
		              ?
                <>
    		<div className="w-100 bg-white  rounded shadow-sm shadow-lg p-5">
	       		
                <div className="my-2 text-center">
			     	<h1>VIEW ALL PRODUCTS</h1>
			    </div>
                
                <div className="my-2 text-right">
                    <Button  variant="danger" className="px-5" onClick={addProductOpen}>ADD PRODUCT</Button>
                </div>
                
                <Table striped bordered hover className="shadow-lg shadow-sm ">
                    <thead className="banner-bg text-light">
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Author</th>
                        <th className="w-25">Description</th>
                        <th>Price</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody sort>
                        {allProducts}
                    </tbody>
                </Table>
             

                {/* ADD PRODUCT MODAL */}

                <Modal
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
                show={modalAdd}
                >
                    <Form onSubmit={e => addProduct(e)}>

                        <Modal.Header className="banner-bg text-light">
                            <Modal.Title>ADD PRODUCT</Modal.Title>
                        </Modal.Header>

                        <Modal.Body>
                            <Row>
                                <Col xs={6} md={6} lg={6}>
                                    <Image className="editModal-img-fit mb-2"
                                        src={imgSource}
                                    />
                                    <Form.Group controlId="title" className="mb-3">
                                        <Form.Label>Title</Form.Label>
                                        <Form.Control
                                            type="text"
                                            placeholder="Product Title"
                                            value={title}
                                            onChange={e => setTitle(e.target.value)}
                                            required
                                        />
                                    </Form.Group>
                                    <Form.Group controlId="author" className="mb-3">
                                        <Form.Label>Author</Form.Label>
                                        <Form.Control
                                            type="text"
                                            placeholder="Product Author"
                                            value={author}
                                            onChange={e => setAuthor(e.target.value)}
                                            required
                                        />
                                    </Form.Group>
                                </Col>
                                
                                <Col xs={6} md={6} lg={6}>
                                    <Form.Group controlId="description" className="mb-3">
                                        <Form.Label>Description</Form.Label>
                                        <Form.Control
                                            as="textarea"
                                            rows={3}
                                            placeholder="Product Description"
                                            value={description}
                                            onChange={e => setDescription(e.target.value)}
                                            required
                                        />
                                    </Form.Group>

                                    <Form.Group controlId="price" className="mb-3">
                                        <Form.Label>Price</Form.Label>
                                        <Form.Control
                                            type="number"
                                            placeholder="Product Price"
                                            value={price}
                                            onChange={e => setPrice(e.target.value)}
                                            required
                                        />
                                    </Form.Group>

                                    <Form.Group controlId="imgSource" className="mb-3">
                                        <Form.Label>Image Link</Form.Label>
                                        <Form.Control
                                            type="text"
                                            placeholder="Product Image Link"
                                            value={imgSource}
                                            onChange={e => setImgSource(e.target.value)}
                                            required
                                        />
                                    </Form.Group>
                                </Col>
                            </Row>
                        </Modal.Body>

                        <Modal.Footer>
                            {isActive
                                ?
                                <Button variant="primary" type="submit" id="submitBtn">
                                    ADD PRODUCT
                                </Button>
                                :
                                <Button variant="danger" type="submit" id="submitBtn" disabled>
                                    ADD PRODUCT
                                </Button>
                            }
                            <Button variant="secondary" onClick={addProductClose}>
                                Close
                            </Button>
                        </Modal.Footer>

                    </Form>
                </Modal>
                
                {/* EDIT MODAL */}

                <Modal
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
                show={modalEdit}
                >
                <Form onSubmit={e => editProduct(e)}>

                        <Modal.Header className="banner-bg text-light">
                            <Modal.Title>EDIT PRODUCT</Modal.Title>
                        </Modal.Header>

                        <Modal.Body>
                            <Image className="editModal-img-fit mb-2"
                                src={imgSource}
                            />
                            <Form.Group controlId="title" className="mb-3">
                                <Form.Label>Title</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Enter Title"
                                    value={title}
                                    onChange={e => setTitle(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group controlId="author" className="mb-3">
                                <Form.Label>Author</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Enter Author"
                                    value={author}
                                    onChange={e => setAuthor(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group controlId="description" className="mb-3">
                                <Form.Label>Description</Form.Label>
                                <Form.Control
                                    as="textarea"
                                    rows={5}
                                    placeholder="Enter Product Description"
                                    value={description}
                                    onChange={e => setDescription(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group controlId="price" className="mb-3">
                                <Form.Label>Product Price</Form.Label>
                                <Form.Control
                                    type="number"
                                    placeholder="Enter Product Price"
                                    value={price}
                                    onChange={e => setPrice(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group controlId="imgSource" className="mb-3">
                                <Form.Label>Image Link</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Enter Product Image Link"
                                    value={imgSource}
                                    onChange={e => setImgSource(e.target.value)}
                                    required
                                />
                            </Form.Group>
                        </Modal.Body>

                        <Modal.Footer>
                            {isActive
                                ?
                                <Button variant="primary" type="submit" id="submitBtn">
                                    Save
                                </Button>
                                :
                                <Button variant="danger" type="submit" id="submitBtn" disabled>
                                    Save
                                </Button>
                            }
                            <Button variant="secondary" onClick={closeEdit}>
                                Close
                            </Button>
                        </Modal.Footer>
                    </Form>
                </Modal>
		</div>
        </>
		:
		<Navigate to="/" />}

        </Col>
        
        </Row>
    </Container>
    
    </>
	)
}