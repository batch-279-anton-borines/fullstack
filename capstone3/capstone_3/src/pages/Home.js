import { Navigate } from "react-router-dom";
import { useContext } from "react"
import UserContext from "../UserContext";
import HomeBanner from "../components/HomeBanner";
import HomeCarousel from "../components/HomeCarousel";
import logo from "../images/logo.png";

export default function Home(){

	
	const { user } = useContext(UserContext);
	const data = {
		title: "EPIC PAGES",
		content: "Feed your Mind on the GO!",
		destination: "/products",
		label: "Begin Reading Now!",
		image: {logo}
	}

	return(
		<>
		{
        (user.isAdmin)
		?
		<Navigate to="/dashboard" />
		:
		<div className="p-3">
			<HomeCarousel/>
			<HomeBanner bannerProp={data}/>
		</div>
		}
		</>
		
	)
}