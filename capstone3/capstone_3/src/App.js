import './App.css';
// components
import AppNavbar from ".//components/AppNavbar";

// pages
import Home from "./pages/Home";
import Register from "./pages/Register";
import Dashboard from "./pages/Dashboard";
import Login from './pages/Login';
import Logout from './pages/Logout';
import Products from "./pages/Products";
import Order from './pages/Order';
import ErrorPage from "./pages/ErrorPage";
import AllProducts from "./pages/AllProducts"

// react-router-dom
import { Route, Routes } from "react-router-dom";
import { BrowserRouter as Router } from "react-router-dom";
import { Container } from "react-bootstrap";
import { useState, useEffect } from "react";
import { UserProvider } from "./UserContext";

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () =>{
    localStorage.clear(); 
  }

  useEffect(()=>{
    console.log(user);
    console.log(localStorage);
  }, [user])
  useEffect(()=>{

    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers:{
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      if(data._id !== undefined){
        setUser({
            id: data._id,
            firstName : data.firstName,
            lastName : data.lastName,
            email : data.email,
            isAdmin: data.isAdmin,
            mobileNo : data.mobileNo,
            orders : data.orders
        });
      }
      else{
        setUser({          
          id: null,
          isAdmin: null
        });
      }
      
    })

  }, [])
  console.log(user)

  return (
  <UserProvider value={{user, setUser, unsetUser}}>
      <Router>    
          <AppNavbar />
          <Container fluid className='p-0 m-0'>
            <Routes>
              <Route exact path="/" element={<Home />} />
              <Route exact path="/register" element={<Register />} />
              <Route exact path="/dashboard" element={<Dashboard />} />
              <Route exact path="/login" element={<Login />} />
              <Route exact path="/logout" element={<Logout />} />
              <Route exact path="/products/allProducts" element={<AllProducts/>} />
              <Route exact path="/products" element={<Products/>} />
              <Route exact path="/products/buy/:productId" element={<Order />} />
              <Route exact path="*" element={<ErrorPage />} />
            </Routes>
          </Container>
      </Router>
  </UserProvider>  
  
  );
}

export default App;