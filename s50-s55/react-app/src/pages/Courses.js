// import courseData from "../data/courses"
import CourseCard from '../components/CourseCard'
import { useEffect, useState } from "react"

export default function Courses(){
	// Checks to see if mock data was captured
	// console.log(courseData);
	// console.log(courseData[0]);

	// state that will be used to store courses retrieved from the database

	const [allCourses, setAllCourses] = useState([]);

	// retrieves the courses from the database upon initial render of the "Courses" component

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setAllCourses(data.map(course=>{
				return(
				<CourseCard key={course._id} courseProp={course}/>	
				)
			}))
		})
	})


	return(
		<>
			<h1>Courses</h1>
			{/*Prop making ang prop passing*/}
			{allCourses}
		</>
		)
}